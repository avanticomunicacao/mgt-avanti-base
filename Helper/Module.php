<?php
/**
 * Avanti Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Base Magento 2
 * @category    Avanti
 * @package     Avanti_Base
 *
 * @copyright   Copyright (c) 2019 Avanti Soluções Web. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\Base\Helper;

use Avanti\Base\Model\Serializer;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Module\Dir\Reader;
use SimpleXMLElement;
use Zend\Http\Client\Adapter\Curl as CurlClient;
use Zend\Http\Response as HttpResponse;
use Zend\Uri\Http as HttpUri;
use Magento\Framework\Json\DecoderInterface;

class Module
{
    const EXTENSIONS_PATH = 'avantibase_extensions';

    const URL_EXTENSIONS = '';

    const ALLOWED_DOMAINS = [
    ];

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var CurlClient
     */
    protected $curlClient;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var array
     */
    protected $restrictedModules = [
        'Avanti_CommonRules',
        'Avanti_Router'
    ];

    /**
     * @see getModuleInfo
     *
     * @var array
     */
    protected $moduleDataStorage = [];

    /**
     * @var array|null
     */
    private $modulesData = null;

    /**
     * @var Reader
     */
    private $moduleReader;

    /**
     * @var File
     */
    private $filesystem;

    /**
     * @var DecoderInterface
     */
    private $jsonDecoder;

    /**
     * @var Escaper
     */
    private $escaper;

    public function __construct(
        Serializer $serializer,
        CacheInterface $cache,
        Reader $moduleReader,
        File $filesystem,
        DecoderInterface $jsonDecoder,
        CurlClient $curl,
        Escaper $escaper
    ) {
        $this->cache = $cache;
        $this->serializer = $serializer;
        $this->curlClient = $curl;
        $this->moduleReader = $moduleReader;
        $this->filesystem = $filesystem;
        $this->jsonDecoder = $jsonDecoder;
        $this->escaper = $escaper;
    }

    /**
     * Get array with info about all Avanti Magento2 Extensions
     *
     * @return bool|mixed
     */
    public function getAllExtensions()
    {
        $serialized = $this->cache->load(self::EXTENSIONS_PATH);
        if ($serialized === false) {
            $this->reload();
            $serialized = $this->cache->load(self::EXTENSIONS_PATH);
        }
        $result = $this->serializer->unserialize($serialized);

        return $result;
    }

    /**
     * Save extensions data to magento cache
     */
    public function reload()
    {
        $feedData = [];
        $feedXml = $this->getFeedData();
        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            $marketplaceOrigin = $this->isOriginMarketplace();

            foreach ($feedXml->channel->item as $item) {
                $code = $this->escaper->escapeHtml((string)$item->code);

                if (!isset($feedData[$code])) {
                    $feedData[$code] = [];
                }

                $title = $this->escaper->escapeHtml((string)$item->title);

                $productPageLink = $marketplaceOrigin ? $item->market_link : $item->link;

                if (!$this->validateLink($productPageLink) || !$this->validateLink($item->guide)) {
                    continue;
                }

                $feedData[$code][$title] = [
                    'name'               => $title,
                    'url'                => $this->escaper->escapeUrl((string)($productPageLink)),
                    'version'            => $this->escaper->escapeHtml((string)$item->version),
                    'conflictExtensions' => $this->escaper->escapeHtml((string)$item->conflictExtensions),
                    'guide'              => $this->escaper->escapeUrl((string)$item->guide),
                ];
            }

            if ($feedData) {
                $this->cache->save($this->serialize($feedData), self::EXTENSIONS_PATH);
            }
        }
    }

    /**
     * Read data from xml file with curl
     *
     * @return bool|SimpleXMLElement
     */
    protected function getFeedData()
    {
        try {
            $curlClient = $this->getCurlClient();

            $location = self::URL_EXTENSIONS;
            $uri = new HttpUri($location);

            $curlClient->setOptions(
                [
                    'timeout' => 8
                ]
            );

            $curlClient->connect($uri->getHost(), $uri->getPort());
            $curlClient->write('GET', $uri, 1.0);
            $data = HttpResponse::fromString($curlClient->read());

            $curlClient->close();

            $xml = new SimpleXMLElement($data->getContent());
        } catch (\Exception $e) {
            return false;
        }

        return $xml;
    }

    /**
     * Returns the cURL client that is being used.
     *
     * @return CurlClient
     */
    public function getCurlClient()
    {
        if ($this->curlClient === null) {
            $this->curlClient = new CurlClient();
        }

        return $this->curlClient;
    }

    public function serialize($data)
    {
        return $this->serializer->serialize($data);
    }

    /**
     * @return array
     */
    public function getRestrictedModules()
    {
        return $this->restrictedModules;
    }

    /**
     * Read info about extension from composer json file
     *
     * @param string $moduleCode
     *
     * @return mixed
     */
    public function getModuleInfo($moduleCode)
    {
        if (!isset($this->moduleDataStorage[$moduleCode])) {
            $this->moduleDataStorage[$moduleCode] = [];

            try {
                $dir = $this->moduleReader->getModuleDir('', $moduleCode);
                $file = $dir . '/composer.json';

                $string = $this->filesystem->fileGetContents($file);
                $this->moduleDataStorage[$moduleCode] = $this->jsonDecoder->decode($string);
            } catch (FileSystemException $e) {
                $this->moduleDataStorage[$moduleCode] = [];
            }
        }

        return $this->moduleDataStorage[$moduleCode];
    }

    /**
     * @param string $moduleCode
     *
     * @return array
     */
    public function getFeedModuleData($moduleCode)
    {
        $moduleData = [];
        if ($this->modulesData === null || $this->modulesData === false) {
            $this->modulesData = $this->getAllExtensions();
        }

        if ($this->modulesData && isset($this->modulesData[$moduleCode])) {
            $module = $this->modulesData[$moduleCode];
            if ($module && is_array($module)) {
                $module = array_shift($module);
            }

            $moduleData = $module;
        }

        return $moduleData;
    }

    /**
     * Check whether module was installed via Magento Marketplace
     *
     * @param string $moduleCode
     *
     * @return bool
     */
    public function isOriginMarketplace($moduleCode = 'Avanti_Base')
    {
        $moduleInfo = $this->getModuleInfo($moduleCode);
        $origin = isset($moduleInfo['extra']['origin']) ? $moduleInfo['extra']['origin'] : null;

        return 'marketplace' === $origin;
    }

    /**
     * @param string $link
     *
     * @return bool
     */
    public function validateLink($link)
    {
        if (! (string) $link) { // fix for xml object
            return true;
        }

        foreach (static::ALLOWED_DOMAINS as $allowedDomain) {
            if (preg_match('/^http[s]?:\/\/' . $allowedDomain . '\/.*$/', $link) === 1) {
                return true;
            }
        }

        return false;
    }
}
