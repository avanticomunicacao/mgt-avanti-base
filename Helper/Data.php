<?php
namespace Avanti\Base\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;

class Data extends AbstractHelper
{
    protected $productRepository;
    protected $storeManagerInterface;
    protected $registry;

    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManagerInterface,
        Registry $registry
    ) {
        $this->productRepository = $productRepository;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->registry = $registry;
        parent::__construct($context);
    }

    public function getProduct($id)
    {
        return $this->productRepository->getById($id);
    }

    public function getImageHover($productId)
    {
        $product = $this->getProduct($productId);
        if ($product->getImageHover()) {
            $imageHover = $product->getImageHover();
            $urlMedia = $this->storeManagerInterface->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $imageUrl = $urlMedia .  'catalog/product/' . $imageHover;
            return $imageUrl;
        }
        return null;
    }

    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }


}
