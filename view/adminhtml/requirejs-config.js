/**
* @author Avanti Team
* @copyright Copyright (c) 2019 Avanti (https://www.avanti.com)
* @package Avanti_Base
*/
var config = {
    config: {
        mixins: {
            'js/theme': {
                'Avanti_Base/js/theme': true
            }
        }
    }
};
